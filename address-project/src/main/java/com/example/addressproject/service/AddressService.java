package com.example.addressproject.service;

import com.example.addressproject.dto.AddressDTO;
import com.example.addressproject.dto.StateDTO;
import com.example.addressproject.model.Address;
import com.example.addressproject.model.Person;
import com.example.addressproject.model.State;
import com.example.addressproject.repository.AddressRepository;
import com.example.addressproject.repository.PersonRepository;
import com.example.addressproject.request.AddressRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;
import org.springframework.web.server.ResponseStatusException;

import javax.annotation.PostConstruct;

@Service
/***
 * Address service for address related functionality
 */
public class AddressService extends BaseService<Address, AddressRepository> {
  /** Manage Address Repo */
  @Autowired AddressRepository addressRepository;

  @Autowired PersonRepository personRepository;
  /** State service for state functionality */
  @Autowired StateService stateService;

  @PostConstruct
  public void setVariables() {
    this.setRepository(addressRepository);
  }

  /**
   * Finds an address by an id
   *
   * @param addressId address id to return
   * @return
   */
  public ResponseEntity<?> getAddressById(Long addressId) {
    if (!ObjectUtils.isEmpty(addressId) && addressRepository.existsById(addressId))
      return ResponseEntity.status(HttpStatus.OK)
          .body(getAddressDTO(addressRepository.findById(addressId).get()));
    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Address Id provided is not valid.");
  }

  /**
   * Adds an address to an existing person
   *
   * @param addressRequest
   * @return
   */
  @Transactional
  public ResponseEntity<?> addAddressToPerson(AddressRequest addressRequest) {
    if (addressRequest.getPersonId() != null
        && personRepository.existsById(addressRequest.getPersonId())) {
      Address address = createNewAddress(addressRequest);
      Person person = personRepository.findById(addressRequest.getPersonId()).get();
      person.setAddress(address);
      personRepository.save(person);
      this.save(address);
      return ResponseEntity.status(HttpStatus.OK).body(address);
    }
    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Invalid person id provided.");
  }

  /**
   * Updates an address assigned to a person.
   *
   * @param addressRequest
   * @return
   */
  public ResponseEntity<?> updateAddressToPerson(AddressRequest addressRequest) {
    if (!ObjectUtils.isEmpty(addressRequest.getId())
        && addressRepository.existsById(addressRequest.getId())) {
      return ResponseEntity.status(HttpStatus.OK)
          .body(getAddressDTO(this.updateAddress(addressRequest)));
    }
    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Invalid address id provided");
  }

  /***
   * Pulls address DTO from address
   * @param address Address object from which to build the DTO
   * @return AddressDTO created
   */
  public AddressDTO getAddressDTO(Address address) {
    StateDTO stateDTO = stateService.getStateDTO(address.getStateId());
    return AddressDTO.builder()
        .id(address.getId())
        .zipCode(address.getZipCode())
        .stateDTO(stateDTO)
        .status(address.getStatus())
        .build();
  }

  /**
   * Creates a new address with Active state by default
   *
   * @param addressRequest request to create the new address
   * @return Address that was created
   */
  @Transactional
  public Address createNewAddress(AddressRequest addressRequest) {
    System.out.println("GOING TO LLOKUP THE FOLLOWING STATE ID: " + addressRequest.getStateId());
    State state = stateService.findById(addressRequest.getStateId()).get();
    System.out.println(state.getName());
    Address address = new Address();
    address.setStatus("ACTIVE");
    address.setStateId(state != null ? state : null);
    address.setZipCode(addressRequest.getZipCode());
    System.out.println(address.getZipCode());
    return this.save(address);
  }

  /***
   * Updates an existing address
   * @param addressRequest request to update the address
   * @return Address updated
   */
  @Transactional
  public Address updateAddress(AddressRequest addressRequest) {
    Address address = this.findById(addressRequest.getId()).get();
    if (address != null) {
      State state = stateService.findById(addressRequest.getStateId()).get();
      address.setStateId(state);
      address.setZipCode(addressRequest.getZipCode());
      return this.save(address);
    }
    throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Invalid address id provided.");
  }

  /**
   * Sets a address as inactive. Updates the person to set the address to null
   *
   * @param addressId Address id to delete
   * @return
   */
  @Transactional
  public ResponseEntity<?> deleteAddress(Long addressId) {
    if (!ObjectUtils.isEmpty(addressId) & this.addressRepository.existsById(addressId)) {
      Address address = this.findById(addressId).get();
      address.setStatus("INACTIVE");
      Person person = address.getPerson();
      person.setAddress(null);
      personRepository.save(person);
      this.save(address);
      return ResponseEntity.status(HttpStatus.OK).body("Address deleted successfully");
    }
    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Invalid address id provided");
  }
}
