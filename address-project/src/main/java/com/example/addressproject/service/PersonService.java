package com.example.addressproject.service;

import com.example.addressproject.dto.PersonDTO;
import com.example.addressproject.model.Address;
import com.example.addressproject.model.Person;
import com.example.addressproject.repository.PersonRepository;
import com.example.addressproject.request.AddressRequest;
import com.example.addressproject.request.PersonRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;
import org.springframework.web.server.ResponseStatusException;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

/** Person Service to create person related functionality */
@Service
public class PersonService extends BaseService<Person, PersonRepository> {
  /** Address service to create/update address. */
  @Autowired AddressService addressService;
  /** Person repository for person list, get, create */
  @Autowired PersonRepository personRepository;

  @PostConstruct
  public void setVariables() {
    this.setRepository(personRepository);
  }

  /**
   * Finds a person by a given id
   *
   * @param personId person id to lookup
   * @return
   */
  public ResponseEntity<?> getPersonById(Long personId) {
    if (!ObjectUtils.isEmpty(personId) && personRepository.existsById(personId)) {
      return ResponseEntity.status(HttpStatus.OK)
          .body(getPersonDTO(personRepository.findById(personId).get()));
    }
    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Person Id provided is not valid.");
  }

  /**
   * Retrieves list of persons in DTO format
   *
   * @return
   */
  public ResponseEntity<?> getPersonDTOList() {
    try {
      List<Person> personList = personRepository.findAll();

      List<PersonDTO> personDTOS = new ArrayList<>();

      personList.forEach(e -> personDTOS.add(getPersonDTO(e)));

      return ResponseEntity.status(HttpStatus.OK).body(personDTOS);
    } catch (Exception e) {
      return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
          .body("An error has occurred when trying to get the list of persons.");
    }
  }

  /**
   * Constructs a personDTO class instance to return to user
   *
   * @param person Person from which to create the DTO
   * @return PersonDTO created
   */
  public PersonDTO getPersonDTO(Person person) {

    try {
      Address address = person.getAddress();
      return PersonDTO.builder()
          .id(person.getId())
          .firstName(person.getFirstName())
          .lastName(person.getLastName())
          .phoneNumber(person.getPhoneNumber())
          .addressDTO(!ObjectUtils.isEmpty(address) ? addressService.getAddressDTO(address) : null)
          .status(person.getStatus())
          .build();
    } catch (Exception e) {
      throw new ResponseStatusException(
          HttpStatus.INTERNAL_SERVER_ERROR,
          "An error has occurred when trying to get person dto.",
          e);
    }
  }

  /**
   * Creates a new Person obj instance
   *
   * @param personRequest data with the person
   * @return Person instance
   */
  public Person buildPersonObj(PersonRequest personRequest) {
    Person person = new Person();
    person.setFirstName(personRequest.getFirstName());
    person.setLastName(personRequest.getLastName());
    person.setStatus("ACTIVE");
    person.setPhoneNumber(personRequest.getPhoneNumber());
    return person;
  }

  /***
   * Creates a person given a request
   * @param personRequest
   * @return Response<>
   */
  @Transactional
  public ResponseEntity<?> createPerson(PersonRequest personRequest) {
    try {
      if (ObjectUtils.isEmpty(personRequest.getFirstName())) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("First Name is Required");
      }
      // build person
      Person person = buildPersonObj(personRequest);
      // Check to see if address exists
      AddressRequest addressRequest = personRequest.getAddress();
      if (addressRequest != null)
        person.setAddress(addressService.createNewAddress(addressRequest));
      return ResponseEntity.status(HttpStatus.OK).body(getPersonDTO(this.save(person)));
    } catch (ResponseStatusException e) {
      throw new ResponseStatusException(
          HttpStatus.INTERNAL_SERVER_ERROR,
          "An error has occurred when trying to create a person.",
          e);
    }
  }

  /**
   * Updates a person based on the request Updates address if address request exists
   *
   * @param personRequest
   * @return
   */
  @Transactional
  public ResponseEntity<?> update(PersonRequest personRequest) {
    try {
      if (ObjectUtils.isEmpty(personRequest.getFirstName())) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
            .body("First Name or Status cannot be empty is required");
      } else {
        // Address
        AddressRequest addressRequest = personRequest.getAddress();

        if (addressRequest == null
            || ObjectUtils.isEmpty(addressRequest.getId())
            || ObjectUtils.isEmpty(personRequest.getId())
            || !personRepository.existsById(personRequest.getId())) {
          return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Invalid ids provided");
        }
        Person person = this.findById(personRequest.getId()).get();
        person.setFirstName(personRequest.getFirstName());
        person.setLastName(personRequest.getLastName());
        person.setPhoneNumber(personRequest.getPhoneNumber());
        // address or null
        person.setAddress(
            addressRequest != null ? addressService.updateAddress(addressRequest) : null);
        this.save(person);
        return ResponseEntity.status(HttpStatus.OK).body(getPersonDTO(person));
      }
    } catch (Exception e) {
      throw new ResponseStatusException(
          HttpStatus.INTERNAL_SERVER_ERROR,
          "An error has occurred when trying to update a person entry.",
          e);
    }
  }

  /**
   * Delete a person. Delete Address. Change status and
   *
   * @param personId
   * @return
   */
  @Transactional
  public ResponseEntity<?> deletePerson(Long personId) {
    try {
      if (ObjectUtils.isEmpty(personId)) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Id is required to delete");
      } else {
        boolean exists = this.personRepository.existsById(personId);
        if (exists) {
          Person person = this.personRepository.getById(personId);
          Address address = person.getAddress();
          if (address != null) addressService.deleteAddress(person.getAddress().getId());
          person.setStatus("INACTIVE");
          this.save(person);
          return ResponseEntity.status(HttpStatus.OK).body("The person has been deleted");
        } else {
          return ResponseEntity.status(HttpStatus.BAD_REQUEST)
              .body("No Person match found with the given id");
        }
      }
    } catch (Exception e) {
      return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
          .body("An error occurred when trying to delete a person, please try again later");
    }
  }
}
