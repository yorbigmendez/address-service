package com.example.addressproject.service;

import com.example.addressproject.dto.StateDTO;
import com.example.addressproject.model.State;
import com.example.addressproject.repository.StateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service
public class StateService extends BaseService<State, StateRepository> {
  @Autowired StateRepository stateRepository;

  @PostConstruct
  public void setVariables() {
    this.setRepository(stateRepository);
  }

  /**
   * Gets the dtate DTO
   *
   * @param state state to get DTO from
   * @return stateDTO
   */
  public StateDTO getStateDTO(State state) {
    return StateDTO.builder()
        .id(state.getId())
        .name(state.getName())
        .status(state.getStatus())
        .build();
  }
}
