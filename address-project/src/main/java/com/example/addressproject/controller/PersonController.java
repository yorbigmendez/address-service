package com.example.addressproject.controller;

import com.example.addressproject.request.PersonRequest;
import com.example.addressproject.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.bind.annotation.*;

@RestController
@EnableTransactionManagement
@RequestMapping(value = "person")
public class PersonController {
  @Autowired PersonService personService;

  /***
   * GET Method to obtain full list of persons from person service
   * @return ResponseEntity<>
   */
  @RequestMapping(value = "/all", method = RequestMethod.GET)
  public ResponseEntity<?> findAll() {
    return personService.getPersonDTOList();
  }

  /**
   * GET method to find a user by a given id
   *
   * @param personId id to lookup the user with
   * @return ResponseEntity<>
   */
  @RequestMapping(method = RequestMethod.GET)
  public ResponseEntity<?> findById(@RequestParam() Long personId) {
    return personService.getPersonById(personId);
  }

  /**
   * Expose endpoint to create a new person with POST
   *
   * @param personRequest request to create a new person
   * @return ResponseEntity<>
   */
  @RequestMapping(method = RequestMethod.POST)
  public ResponseEntity<?> createPerson(@RequestBody() PersonRequest personRequest) {
    return personService.createPerson(personRequest);
  }

  /**
   * Updates a person
   *
   * @param personRequest Person request that will be updated
   * @return ResponseEntity<>
   */
  @RequestMapping(method = RequestMethod.PUT)
  public ResponseEntity<?> updatePerson(@RequestBody() PersonRequest personRequest) {
    return personService.update(personRequest);
  }

  /**
   * Deletes a person
   *
   * @param personId Person to delete
   * @return ResponseEntity<>
   */
  @RequestMapping(method = RequestMethod.DELETE)
  public ResponseEntity<?> delete(@RequestParam() Long personId) {
    return personService.deletePerson(personId);
  }
}
