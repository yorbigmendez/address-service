package com.example.addressproject.controller;

import com.example.addressproject.request.AddressRequest;
import com.example.addressproject.service.AddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.bind.annotation.*;

@RestController
@EnableTransactionManagement
@RequestMapping(value = "/address")
public class AddressController {
  @Autowired AddressService addressService;

  /**
   * Gets an address by an id
   *
   * @param addressId Address id to get
   * @return
   */
  @RequestMapping(method = RequestMethod.GET)
  public ResponseEntity<?> getAddress(@RequestParam Long addressId) {
    return addressService.getAddressById(addressId);
  }

  /**
   * Deletes an address
   *
   * @param addressId Address id to delete
   * @return
   */
  @RequestMapping(method = RequestMethod.DELETE)
  public ResponseEntity<?> deleteAddress(@RequestParam Long addressId) {
    return addressService.deleteAddress(addressId);
  }

  /***
   * Adds an address to an existing person
   * @param addressRequest
   * @return ResponseEntity
   */
  @RequestMapping(method = RequestMethod.POST)
  public ResponseEntity<?> addAddressToPerson(@RequestBody AddressRequest addressRequest) {
    return addressService.addAddressToPerson(addressRequest);
  }

  @RequestMapping(method = RequestMethod.PUT)
  public ResponseEntity<?> updateAddressToPerson(@RequestBody AddressRequest addressRequest) {
    return addressService.updateAddressToPerson(addressRequest);
  }
}
