package com.example.addressproject.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;

@Getter
@Setter
@Entity
@SQLDelete(sql = "UPDATE person SET status=INACTIVE WHERE id=?")
@Where(clause = "status = 'ACTIVE'")
public class Person {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id")
  private Long id;

  @Column(name = "first_name")
  private String firstName;

  @Column(name = "last_name")
  private String lastName;

  @Column(name = "phone_number")
  private String phoneNumber;

  @Column(name = "status")
  private String status;

  @ManyToOne
  @JoinColumn(name = "address_id")
  private Address address;
  //
  //    private
  //
  //    @PrePersist
  //    public void prePersist() {
  //        this.setVersion(1);
  //        this.setCreated(new Date());
  //    }
  //
  //    @PreUpdate
  //    public void preUpdate() {
  //        this.setVersion(this.getVersion() + 1);
  //        this.setUpdated(new Date());
  //    }
}
