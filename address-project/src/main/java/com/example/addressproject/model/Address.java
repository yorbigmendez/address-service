package com.example.addressproject.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;

@Getter
@Setter
@Entity
@SQLDelete(sql = "UPDATE address SET status=INACTIVE WHERE id=?")
@Where(clause = "status = 'ACTIVE'")
public class Address {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id")
  private Long id;

  @Column(name = "zip_code")
  private Integer zipCode;

  @Column(name = "status")
  private String status;

  @ManyToOne
  @JoinColumn(name = "state_id")
  private State stateId;

  @OneToOne(mappedBy = "address")
  private Person person;
}
