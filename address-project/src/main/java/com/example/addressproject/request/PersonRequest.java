package com.example.addressproject.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PersonRequest {
  private Long id;
  private String firstName;
  private String lastName;
  private String phoneNumber;
  private AddressRequest address;
  private String status;
}
