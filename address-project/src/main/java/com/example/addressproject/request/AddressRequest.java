package com.example.addressproject.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AddressRequest {
  private Long id;
  private Integer zipCode;
  private Long stateId;
  private String status;
  private Long personId;
}
