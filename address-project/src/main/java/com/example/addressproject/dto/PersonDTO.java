package com.example.addressproject.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class PersonDTO {
  private Long id;
  private String firstName;
  private String lastName;
  private String phoneNumber;
  private AddressDTO addressDTO;
  private String status;
}
