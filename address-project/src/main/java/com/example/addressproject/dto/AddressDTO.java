package com.example.addressproject.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class AddressDTO {
  private Long id;
  private Integer zipCode;
  private StateDTO stateDTO;
  private String status;
}
