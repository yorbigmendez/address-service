package com.example.addressproject.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class StateDTO {
  private Long id;
  private String name;
  private String status;
}
