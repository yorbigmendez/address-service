--CREATE SCHEMA IF NOT EXISTS test AUTHORIZATION sa;

DROP TABLE IF EXISTS state;
CREATE TABLE state (
    id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(250) NOT NULL,
    status VARCHAR(10) NOT NULL
--    version NOT NULL,
--    updated_at DATE NULL,
--    created_at DATE NOT NULL,
 );

DROP TABLE IF EXISTS address;

CREATE TABLE address (
    id INT AUTO_INCREMENT PRIMARY KEY,
    zip_code INT NOT NULL,
    state_id INT NOT NULL,
    status VARCHAR(10) NOT NULL,
    --     version NOT NULL,
    --     updated_at DATE NULL,
    --     created_at DATE NOT NULL,
    CONSTRAINT fk_address_state FOREIGN KEY (state_id) REFERENCES state(id)
 );

DROP TABLE IF EXISTS person;

CREATE TABLE person (
    id INT AUTO_INCREMENT PRIMARY KEY,
    first_name VARCHAR(50) NOT NULL,
    last_name VARCHAR(50) NULL,
    phone_number VARCHAR(25) NULL,
    address_id INT NULL,
    status VARCHAR(10) NOT NULL,
--    version NOT NULL,
--    updated_at DATE NULL,
--    created_at DATE NOT NULL,
    CONSTRAINT fk_person_address FOREIGN KEY (address_id) REFERENCES address(id)
);

INSERT INTO state (name, status)  VALUES ('Alabama', 'ACTIVE'),
                                        ('Alaska', 'ACTIVE'),
                                        ('Arizona', 'ACTIVE'),
                                        ('Arkansas', 'ACTIVE'),
                                        ('California', 'ACTIVE'),
                                        ('Colorado', 'ACTIVE'),
                                        ('Connecticut', 'ACTIVE'),
                                        ('Delaware', 'ACTIVE'),
                                        ('Florida', 'ACTIVE'),
                                        ('Georgia', 'ACTIVE'),
                                        ('Hawaii', 'ACTIVE'),
                                        ('Idaho', 'ACTIVE'),
                                        ('Illinois', 'ACTIVE'),
                                        ('Indiana', 'ACTIVE'),
                                        ('Iowa', 'ACTIVE'),
                                        ('Kansas', 'ACTIVE'),
                                        ('Kentucky', 'ACTIVE'),
                                        ('Louisiana', 'ACTIVE'),
                                        ('Maine', 'ACTIVE'),
                                        ('Maryland', 'ACTIVE'),
                                        ('Massachussetts', 'ACTIVE'),
                                        ('Michigan', 'ACTIVE'),
                                        ('Minnesota', 'ACTIVE'),
                                        ('Mississippi', 'ACTIVE'),
                                        ('Missouri', 'ACTIVE'),
                                        ('Montana', 'ACTIVE'),
                                        ('Nebraska', 'ACTIVE'),
                                        ('Nevada', 'ACTIVE'),
                                        ('New Hampshire', 'ACTIVE'),
                                        ('New Mexico', 'ACTIVE'),
                                        ('New York', 'ACTIVE'),
                                        ('North Carolina', 'ACTIVE'),
                                        ('North Dakota', 'ACTIVE'),
                                        ('Ohio', 'ACTIVE'),
                                        ('Oklahoma', 'ACTIVE'),
                                        ('Oregon', 'ACTIVE'),
                                        ('Pennsylvania', 'ACTIVE'),
                                        ('Rhode Island', 'ACTIVE'),
                                        ('South Carolina', 'ACTIVE'),
                                        ('South Dakota', 'ACTIVE'),
                                        ('Tennessee', 'ACTIVE'),
                                        ('Texas', 'ACTIVE'),
                                        ('Utah', 'ACTIVE'),
                                        ('Vermont', 'ACTIVE'),
                                        ('Virginia', 'ACTIVE'),
                                        ('Washington', 'ACTIVE'),
                                        ('West Virginia', 'ACTIVE'),
                                        ('Wisconsin', 'ACTIVE'),
                                        ('Wyoming', 'ACTIVE');
